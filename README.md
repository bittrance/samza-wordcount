# Packaged Hello Samza example

This repo is the [Samza Wordcount example](https://github.com/apache/samza-hello-samza/blob/latest/quickstart/wordcount.tar.gz) modernized for a current version Samza and dependencies.

## Starting
```
./gradlew build
docker-compose up -d
./gradlew run --args="--config job.config.loader.factory=org.apache.samza.config.loaders.PropertiesConfigLoaderFactory --config job.config.loader.properties.path=$PWD/src/main/config/word-count.properties"
```

## Testing
kafkacat is a great utility to interact with Kafka. You can test the running wordcounter. First, subscribe to the the results stream:
```
kafkacat -C -b localhost:9093,localhost:9094,localhost:9095 -t word-count-output -u
```
Then you can send input to it:
```
echo 'yada yada' | kafkacat -P -b localhost:9093,localhost:9094,localhost:9095 -t sample-text
```